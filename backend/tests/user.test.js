const dbHandler = require('./db-handler')
const User = require('../models/User')

beforeAll(async () => {
    await dbHandler.connect()
})

afterEach(async () => {
    await dbHandler.clearDatabase()
})

afterAll(async () => {
    await dbHandler.closeDatabase()
})

const userComplete = {
    first_name: "test1",
    last_name: "test1",
    email: "test1@test.com",
    password: "test1"
}
const userErrornullfirstname = {
    first_name: "",
    last_name: "test2",
    email: "test2@test.com",
    password: "test2"
}
const userErrornulllastname = {
    first_name: 'test3',
    last_name: '',
    email: 'test3@test.com',
    password: 'test3'
}
const userErrornullemail = {
    first_name: 'test4',
    last_name: 'test4',
    email: '',
    password: 'test4'
}
const userErrornullpassword = {
    first_name: 'test5',
    last_name: 'test5',
    email: 'test5@test.com',
    password: ''
}
const userErrorIsExist = {
    first_name: 'test6',
    last_name: 'test6',
    email: 'test6@test.com',
    password: 'test6'
}
describe('User', () => {
    it('สามารถเพิ่มข้อมูล user ที่ถูกต้อง ', async () => {
        let error = null
        try{
            const user = new User(userComplete)
            await user.save()
        }catch(e){
            error = e
        }
        expect(error).toBeNull()
    })
    it('ไม่่สามารถเพิ่มข้อมูล ได้เพราะ ไม่มี firstname ', async () => {
        let error = null
        try{
            const user = new User(userErrornullfirstname)
            await user.save()
        }catch(e){
            error = e
        }
        expect(error).not.toBeNull()
    })
    it('ไม่่สามารถเพิ่มข้อมูล ได้เพราะ ไม่มี lastname ', async () => {
        let error = null
        try{
            const user = new User(userErrornulllastname)
            await user.save()
        }catch(e){
            error = e
        }
        expect(error).not.toBeNull()
    })
    it('ไม่่สามารถเพิ่มข้อมูล ได้เพราะ ไม่มี email ', async () => {
        let error = null
        try{
            const user = new User(userErrornullemail)
            await user.save()
        }catch(e){
            error = e
        }
        expect(error).not.toBeNull()
    })
    it('ไม่่สามารถเพิ่มข้อมูล ได้เพราะ ไม่มี password ', async () => {
        let error = null
        try{
            const user = new User(userErrornullpassword)
            await user.save()
        }catch(e){
            error = e
        }
        expect(error).not.toBeNull()
    })
    it('ไม่่สามารถเพิ่มข้อมูลซ้ำได้ ', async () => {
        let error = null
        try{
            const user1 = new User(userErrorIsExist)
            await user1.save()
            const user2 = new User(userErrorIsExist)
            await user2.save()
        }catch(e){
            error = e
        }
        expect(error).not.toBeNull()
    })
})