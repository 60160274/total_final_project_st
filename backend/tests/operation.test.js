const dbHandler = require('./db-handler')
const Operation = require('../models/Operation')

beforeAll(async () => {
    await dbHandler.connect()
})

afterEach(async () => {
    await dbHandler.clearDatabase()
})

afterAll(async () => {
    await dbHandler.closeDatabase()
})

const OperationDeposit = {
    type: "Deposit",
    amount: 200
}
const OperationWithdraw = {
    type: "Withdraw",
    amount: 200
}
const OperationDepositWithDecimal = {
    type: "Deposit",
    amount: 200.5
}
const OperationWithdrawWithDecimal = {
    type: "Withdraw",
    amount: 200.5
}
const OperationDepositErrorAmount = {
    type: "Deposit",
    amount: -1
}
const OperationWithdrawErrorAmount = {
    type: "Withdraw",
    amount: -1
}
const OperationErrorType = {
    type: "Tranfers",
    amount: 200
}
describe('Operation', () => {
    it('สามารถเพิ่มข้อมูล Operation: Typeเป็น Deposit  ', async () => {
        let error = null
        try{
            const ops = new Operation(OperationDeposit)
            await ops.save()
        }catch(e){
            error = e
        }
        expect(error).toBeNull()
    })
    it('สามารถเพิ่มข้อมูล Operation: Typeเป็น Withdraw  ', async () => {
        let error = null
        try{
            const ops = new Operation(OperationWithdraw)
            await ops.save()
        }catch(e){
            error = e
        }
        expect(error).toBeNull()
    })
    it('สามารถเพิ่มข้อมูล Operation: Typeเป็น Deposit : Amount เป็น ทศนิยม  ', async () => {
        let error = null
        try{
            const ops = new Operation(OperationDepositWithDecimal)
            await ops.save()
        }catch(e){
            error = e
        }
        expect(error).toBeNull()
    })
    it('สามารถเพิ่มข้อมูล Operation: Typeเป็น Withdraw : Amount เป็น ทศนิยม  ', async () => {
        let error = null
        try{
            const ops = new Operation(OperationWithdrawWithDecimal)
            await ops.save()
        }catch(e){
            error = e
        }
        expect(error).toBeNull()
    })
    it('ไม่สามารถเพิ่มข้อมูล Operation: Typeเป็น Deposit :Amount เป็น ติดลบ ', async () => {
        let error = null
        try{
            const ops = new Operation(OperationDepositErrorAmount)
            await ops.save()
        }catch(e){
            error = e
        }
        expect(error).not.toBeNull()
    })
    it('ไม่สามารถเพิ่มข้อมูล Operation: Typeเป็น Withdraw :Amount เป็น ติดลบ ', async () => {
        let error = null
        try{
            const ops = new Operation(OperationWithdrawErrorAmount)
            await ops.save()
        }catch(e){
            error = e
        }
        expect(error).not.toBeNull()
    })
    it('ไม่สามารถเพิ่มข้อมูล Operation:Typeเป็น Tranfers ', async () => {
        let error = null
        try{
            const ops = new Operation(OperationErrorType)
            await ops.save()
        }catch(e){
            error = e
        }
        expect(error).not.toBeNull()
    })
})