const express = require("express")
const cors = require("cors")
const bodyParser = require("body-parser")
const app = express()
const mongoose = require("mongoose")
const port = process.env.PORT || 5000

app.use(bodyParser.json())
app.use(cors())
app.use(bodyParser.urlencoded({ extended: false }))

const mongoURI = 'mongodb://admin:pass@localhost/mydb'

mongoose.connect(mongoURI, { useNewUrlParser : true,useUnifiedTopology: true })
    .then(() => console.log("MongoDB Connected"))
    .catch(err => console.log(err))

const Users = require("./routes/Users")
const Operations = require("./routes/operations")

app.use("/users", Users)
app.use("/operations",Operations)

app.listen(port, function () {
    console.log("Server is running on port: " + port)
})
