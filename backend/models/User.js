const mongoose = require("mongoose")
const Schema = mongoose.Schema

const UserSchema = new Schema({
    first_name: {
        type: String,
        required: [true, 'Please Include your name'],
        unique: true
    },
    last_name: {
        type: String,
        required: [true, 'Please Include your name'],
        unique: true 
    },
    email: {
        type: String,
        required: [true, 'Please Include your email'],
        unique: true
    },
    password: {
        type: String,
        required: [true, 'Please Include your password'],
        unique: true
    },
    date: {
        type: Date,
        default: Date.now
    }
})

module.exports = User = mongoose.model('users', UserSchema)