const mongoose = require('mongoose')
const Schema = mongoose.Schema

const operationSchema = new Schema(
  {
    date: {
      type: Date,
      default: Date.now
    },
    type: {
      type: String,
      required: true,
      enum: ['Withdraw', 'Deposit']
    },
    amount: {
      type: Number,
      required: true,
      min: 0
    }
  },
  { collection: 'Operations' }
)

module.exports = mongoose.model(' ', operationSchema)
