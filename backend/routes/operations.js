const express = require('express')
const router = express.Router()
const operationController = require('../Controllers/OperationController')

router.get('/get', operationController.getOperations)

router.post('/add', operationController.addOperation)

module.exports = router
